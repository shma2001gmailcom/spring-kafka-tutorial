package org.misha.userconsumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.misha.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;

@SuppressWarnings("unused")
@Component
public class UserConsumerService {
    private static final Logger log = LoggerFactory.getLogger(UserConsumerService.class);
    public static final String BATCH_DESCRIPTION = "\n=================\nreceived:\n key: {}\n value: {}\n topic:{}" +
            "\n offset: {}\n timestamp: {}";

    @KafkaListener(topics = {"user-topic"}, containerFactory = "kafkaListenerFactory")
    public void consumeUserData(List<ConsumerRecord<String, User>> recordsBatch) {
        System.err.printf("Batch size: %d%n", recordsBatch.size());
        recordsBatch.forEach(record -> {
            TopicPartition topicPartition = new TopicPartition(record.topic(), record.partition());
            User user = record.value();
            log.info(BATCH_DESCRIPTION, record.key(), user, topicPartition, record.offset(), record.timestamp());
            log.info("User constructed by consumer: {}", user);
        });
    }
}

package org.misha.userconsumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.listener.ConsumerAwareRebalanceListener;
import org.springframework.lang.NonNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RebalanceListener implements ConsumerAwareRebalanceListener {
    private final Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();

    @Override
    public void onPartitionsRevokedBeforeCommit(@NonNull Consumer<?, ?> consumer,
                                                @NonNull Collection<TopicPartition> partitions) {
        consumer.commitSync(currentOffsets);
    }

    @Override
    public void onPartitionsRevokedAfterCommit(@NonNull Consumer<?, ?> consumer,
                                               @NonNull Collection<TopicPartition> partitions) {
        partitions.forEach(partition -> currentOffsets.put(partition,
                new OffsetAndMetadata(consumer.position(partition) + 1)));
    }

    @Override
    public void onPartitionsAssigned(@NonNull Consumer<?, ?> consumer, @NonNull Collection<TopicPartition> partitions) {
        if (currentOffsets.isEmpty()) return;
        partitions.forEach(partition -> consumer.seek(partition, currentOffsets.get(partition).offset()));
    }
}

package org.misha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class AppProducer {

    public static void main(String[] args) {
        SpringApplication.run(AppProducer.class, args);
    }
}
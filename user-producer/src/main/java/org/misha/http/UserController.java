package org.misha.http;

import com.fasterxml.jackson.core.type.TypeReference;
import org.misha.User;
import org.misha.userproducer.UserProducerService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.misha.Utils.readJson;

@SuppressWarnings("unused")
@RestController
public class UserController {
    private final UserProducerService userProducerService;

    public UserController(UserProducerService userProducerService) {
        this.userProducerService = userProducerService;
    }

    @PostMapping(path = "/user/post", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void postUser(@RequestBody String json) {
        TypeReference<List<User>> type = new TypeReference<>() {
        };
        readJson(json, type).forEach(userProducerService::sendUserData);
    }
}

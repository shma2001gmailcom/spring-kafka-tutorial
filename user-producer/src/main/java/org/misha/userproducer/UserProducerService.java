package org.misha.userproducer;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.misha.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

@Component
public class UserProducerService {
    private final KafkaTemplate<String, User> kafkaTemplate;
    private final String topic;

    public UserProducerService(KafkaTemplate<String, User> kafkaTemplate,
                               @Value("${user.topic}") String topic
    ) {
        this.kafkaTemplate = kafkaTemplate;
        this.topic = topic;
    }

    public void sendUserData(User user) {
        try {
            SendResult<String, User> result
                    = kafkaTemplate.send(new ProducerRecord<>(topic, user.getName(), user)).get();
            System.out.printf("Record: %s; Meta: %s%n", result.getProducerRecord(), result.getRecordMetadata());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e.getCause());
        }
    }
}

package org.misha.userproducer.partitioner;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.utils.Utils;
import org.misha.User;

import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

//put user into a partition specified by rules
public class UserPartitioner implements Partitioner {
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        List<PartitionInfo> partitions = cluster.availablePartitionsForTopic(topic);
        User user = (User) value;
        return switch (user.getSex()) {
            case 0 -> {
                if (user.getAge() < 40) {
                    yield 0;
                }
                yield 1;
            }
            case 1 -> Math.abs(Utils.murmur2(user.getName().getBytes(UTF_8))) % partitions.size() - 1;
            default -> Math.abs(Utils.murmur2(keyBytes)) % partitions.size() - 1;
        };
    }

    @Override
    public void close() {
    }

    @Override
    public void configure(Map<String, ?> configs) {
    }
}

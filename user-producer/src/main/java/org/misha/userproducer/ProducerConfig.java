package org.misha.userproducer;

import com.google.common.collect.ImmutableMap;
import org.apache.kafka.common.serialization.StringSerializer;
import org.misha.User;
import org.misha.userproducer.partitioner.UserPartitioner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonSerializer;

import static org.apache.kafka.clients.producer.ProducerConfig.BATCH_SIZE_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.LINGER_MS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.PARTITIONER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

@SuppressWarnings("all")
@Configuration
public class ProducerConfig {
    public static final ImmutableMap<String, Object> PARTITIONER_CONF = new ImmutableMap.Builder<String, Object>()
            .put(PARTITIONER_CLASS_CONFIG, UserPartitioner.class.getName())
            .build();
    public static final String STRING_SERIALIZER = StringSerializer.class.getName();
    public static final String JSON_SERIALIZER = JsonSerializer.class.getName();

    @Bean
    public KafkaTemplate<String, User> kafkaTemplate() {
        return new KafkaTemplate<>(defaultKafkaProducerFactory(), PARTITIONER_CONF);
    }

    @Bean
    public DefaultKafkaProducerFactory<String, User> defaultKafkaProducerFactory() {
        ImmutableMap<String, Object> config = new ImmutableMap.Builder<String, Object>()
                .put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
                .put(KEY_SERIALIZER_CLASS_CONFIG, STRING_SERIALIZER)
                .put(VALUE_SERIALIZER_CLASS_CONFIG, JSON_SERIALIZER)
                .put(BATCH_SIZE_CONFIG, 3355443)
                .put(LINGER_MS_CONFIG, 2).build();
        DefaultKafkaProducerFactory<String, User> factory = new DefaultKafkaProducerFactory<>(config);
        return factory;
    }
}

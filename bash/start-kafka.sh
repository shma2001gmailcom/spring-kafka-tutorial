#!/usr/bin/env bash

export $( cat bash/environment.properties | xargs )
echo ${kafka_home}
bash ${kafka_home}/bin/kafka-server-start.sh ${kafka_home}/config/server.properties
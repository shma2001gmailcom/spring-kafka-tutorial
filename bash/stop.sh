#!/usr/bin/env bash

export $( cat bash/environment.properties | xargs )
echo ${kafka_home}
${kafka_home}/bin/kafka-server-stop.sh && ${kafka_home}/bin/zookeeper-server-stop.sh